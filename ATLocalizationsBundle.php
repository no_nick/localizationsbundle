<?php

namespace AT\LocalizationsBundle;

use AT\LocalizationsBundle\Entity\Country;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ATLocalizationsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $modelDir = realpath(__DIR__.'/Resources/config/doctrine');
        $mappings = array(
//            $modelDir => Country::class,
            $modelDir => 'AT\LocalizationBundle\Entity',
        );

        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(
                DoctrineOrmMappingsPass::createXmlMappingDriver(
                    $mappings,
                    array('at_localizations.event_manager_name'),
                    'at_localizations.backend_type_orm'
                )
            );
        }
    }

}
