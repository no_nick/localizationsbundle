<?php

namespace AT\LocalizationsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AT\LocalizationsBundle\Entity\Province;

class ProvinceTest extends WebTestCase
{
    public function testProvinceName()
    {
        $province = $this->getProvince();
        $this->assertNull($province->getName());

        $province->setName('lubelskie');
        $this->assertSame('lubelskie', $province->getName());
    }

    public function testRelationUpdate()
    {
        $countryMock = $this->createMock('AT\LocalizationsBundle\Entity\Country');

        $province = $this->getProvince();
        $this->assertNull($province->getCountry());

        $province->setCountry($countryMock);
        $this->assertInstanceOf('AT\LocalizationsBundle\Entity\CountryInterface', $province->getCountry());
    }

    public function testManagingCities()
    {
        $province = $this->getProvince();
        $mockCity = $this->createMock('AT\LocalizationsBundle\Entity\City');
        $mockCity2 = $this->createMock('AT\LocalizationsBundle\Entity\City');
        $citiesArray = [
            $mockCity,
            $mockCity2
        ];

        $this->assertInstanceOf('AT\LocalizationsBundle\Entity\City', $mockCity);

        $this->assertEmpty($province->getCities());
        $this->assertFalse($province->hasCities());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $province->getCities());
        $this->assertFalse($province->hasCity($mockCity));

        $province->addCity($mockCity);
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $province->getCities());
        $this->assertNotEmpty($province->getCities());
        $this->assertTrue($province->hasCity($mockCity));
        $this->assertTrue($province->hasCities());

        $province->removeCity($mockCity);
        $this->assertEmpty($province->getCities());
        $this->assertFalse($province->hasCity($mockCity));
        $this->assertFalse($province->hasCities());

        $province->setCities($citiesArray);
        $this->assertNotEmpty($province->getCities());
        $this->assertTrue($province->hasCity($mockCity2));
        $this->assertTrue($province->hasCities());
    }

    /**
     * @return Province
     */
    protected function getProvince()
    {
        return $this->getMockForAbstractClass('AT\LocalizationsBundle\Entity\Province');
    }
}
