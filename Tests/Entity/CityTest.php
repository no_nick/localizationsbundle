<?php

namespace AT\LocalizationsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AT\LocalizationsBundle\Entity\City;

class CityTest extends WebTestCase
{
    public function testProvinceName()
    {
        $city = $this->getCity();
        $this->assertNull($city->getName());

        $city->setName('Lublin');
        $this->assertSame('Lublin', $city->getName());
    }

    public function testRelationUpdate()
    {
        $provinceMock = $this->createMock('AT\LocalizationsBundle\Entity\Province');

        $city = $this->getCity();
        $this->assertNull($city->getProvince());

        $city->setProvince($provinceMock);
        $this->assertInstanceOf('AT\LocalizationsBundle\Entity\ProvinceInterface', $city->getProvince());
    }

    /**
     * @return City
     */
    protected function getCity()
    {
        return $this->getMockForAbstractClass('AT\LocalizationsBundle\Entity\City');
    }
}
