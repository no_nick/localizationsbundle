<?php

namespace AT\LocalizationsBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AT\LocalizationsBundle\Entity\Country;

class CountryTest extends WebTestCase
{
    public function testCountryName()
    {
        $country = $this->getCountry();
        $this->assertNull($country->getName());

        $country->setName('Polska');
        $this->assertSame('Polska', $country->getName());
    }

    public function testManagingProvinces()
    {
        $country = $this->getCountry();
        $mockProvince = $this->createMock('AT\LocalizationsBundle\Entity\Province');
        $mockProvince2 = $this->createMock('AT\LocalizationsBundle\Entity\Province');
        $provincesArray = [
            $mockProvince,
            $mockProvince2
        ];

        $this->assertInstanceOf('AT\LocalizationsBundle\Entity\Province', $mockProvince);

        $this->assertEmpty($country->getProvinces());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $country->getProvinces());
        $this->assertFalse($country->hasProvince($mockProvince));
        $this->assertFalse($country->hasProvinces());

        $country->addProvince($mockProvince);
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $country->getProvinces());
        $this->assertNotEmpty($country->getProvinces());
        $this->assertTrue($country->hasProvince($mockProvince));
        $this->assertTrue($country->hasProvinces());

        $country->removeProvince($mockProvince);
        $this->assertEmpty($country->getProvinces());
        $this->assertFalse($country->hasProvince($mockProvince));
        $this->assertFalse($country->hasProvinces());

        $country->setProvinces($provincesArray);
        $this->assertNotEmpty($country->getProvinces());
        $this->assertTrue($country->hasProvince($mockProvince2));
        $this->assertTrue($country->hasProvinces());
    }

    /**
     * @return Country
     */
    protected function getCountry()
    {
        return $this->getMockForAbstractClass('AT\LocalizationsBundle\Entity\Country');
    }
}
