<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 17:29
 */

namespace AT\LocalizationsBundle\Entity;


interface CityableInterface
{
    /**
     * Gets the cities assigned to province
     *
     * @return \Traversable
     */
    public function getCities();

    /**
     * Add a City to the object
     *
     * @param CityInterface $city
     * @return self
     */
    public function addCity(CityInterface $city);

    /**
     * Remove the city from the cities
     *
     * @param CityInterface $city
     * @return self
     */
    public function removeCity(CityInterface $city);

    /**
     * Indicates whether city is assigned to the specified object or not
     *
     * @param CityInterface $city
     * @return bool
     */
    public function hasCity(CityInterface $city);

    /**
     * Sets an collection of cities
     *
     * @param array $cities
     * @return self
     */
    public function setCities(array $cities);

    /**
     * Indicates whether any cities are assigned or not
     *
     * @return bool
     */
    public function hasCities();
}