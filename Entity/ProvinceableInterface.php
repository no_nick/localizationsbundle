<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 16:07
 */

namespace AT\LocalizationsBundle\Entity;


interface ProvinceableInterface
{
    /**
     * Gets the provinces assigned to country
     *
     * @return \Traversable
     */
    public function getProvinces();

    /**
     * Indicates whether province is assigned to the specified country or not
     *
     * @param ProvinceInterface $province
     * @return bool
     */
    public function hasProvince(ProvinceInterface $province);

    /**
     * Add a province to the provinces
     *
     * @param ProvinceInterface $province
     * @return self
     */
    public function addProvince(ProvinceInterface $province);

    /**
     * Remove the province from the provinces
     *
     * @param ProvinceInterface $province
     * @return self
     */
    public function removeProvince(ProvinceInterface $province);

    /**
     * Sets an collection of provinces
     *
     * @param array $provinces
     * @return self
     */
    public function setProvinces(array $provinces);

    /**
     * Indicates whether any provinces are assigned or not
     *
     * @return bool
     */
    public function hasProvinces();
}