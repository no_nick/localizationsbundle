<?php

namespace AT\LocalizationsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Province
 */
abstract class Province implements ProvinceInterface, CityableInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var CountryInterface
     */
    protected $country;

    /**
     * @var Collection
     */
    protected $cities;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * {@inheritdoc}
     */
    public function setCountry(CountryInterface $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCities()
    {
        return $this->cities ?: $this->cities = new ArrayCollection();
    }

    public function addCity(CityInterface $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    public function removeCity(CityInterface $city)
    {
        if ($this->getCities()->contains($city)) {
            $this->getCities()->removeElement($city);
        }

        return $this;
    }

    public function hasCity(CityInterface $city)
    {
        return $this->getCities()->contains($city);
    }

    public function setCities(array $cities)
    {
        $this->cities = new ArrayCollection($cities);
    }

    public function hasCities()
    {
        return !$this->getCities()->isEmpty();
    }
}

