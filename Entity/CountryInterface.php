<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 19.12.16
 * Time: 23:11
 */

namespace AT\LocalizationsBundle\Entity;


use AT\LocalizationsBundle\Component\NameableInterface;
use AT\LocalizationsBundle\Component\ResourceInterface;

interface CountryInterface extends
    ResourceInterface,
    NameableInterface
{

}