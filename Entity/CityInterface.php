<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 02.01.17
 * Time: 17:14
 */

namespace AT\LocalizationsBundle\Entity;


use AT\LocalizationsBundle\Component\NameableInterface;
use AT\LocalizationsBundle\Component\ResourceInterface;

interface CityInterface extends
    NameableInterface,
    ResourceInterface
{
    /**
     * Sets the province where city belongs to
     *
     * @param ProvinceInterface $province
     * @return self
     */
    public function setProvince(ProvinceInterface $province);

    /**
     * Gets province where city belongs to
     *
     * @return ProvinceInterface|null
     */
    public function getProvince();
}