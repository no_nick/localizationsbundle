<?php

namespace AT\LocalizationsBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Country
 */
abstract class Country implements CountryInterface, ProvinceableInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Collection
     */
    protected $provinces;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getProvinces()
    {
        return $this->provinces ?: $this->provinces = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function hasProvince(ProvinceInterface $province)
    {
        return $this->getProvinces()->contains($province);
    }

    /**
     * {@inheritdoc}
     */
    public function addProvince(ProvinceInterface $province)
    {
        $this->provinces[] = $province;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeProvince(ProvinceInterface $province)
    {
        if ($this->getProvinces()->contains($province)) {
            $this->getProvinces()->removeElement($province);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setProvinces(array $provinces)
    {
        $this->provinces = new ArrayCollection($provinces);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function hasProvinces()
    {
        return !$this->getProvinces()->isEmpty();
    }
}

