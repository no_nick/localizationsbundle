<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 20.12.16
 * Time: 21:50
 */

namespace AT\LocalizationsBundle\Entity;


use AT\LocalizationsBundle\Component\NameableInterface;
use AT\LocalizationsBundle\Component\ResourceInterface;

interface ProvinceInterface extends
    ResourceInterface,
    NameableInterface
{
    /**
     * Gets the country where object belongs to
     *
     * @return CountryInterface|null
     */
    public function getCountry();

    /**
     * @param CountryInterface $country
     * @return self
     */
    public function setCountry(CountryInterface $country);
}