<?php
/**
 * Created by PhpStorm.
 * User: aterepora
 * Date: 20.12.16
 * Time: 21:49
 */

namespace AT\LocalizationsBundle\Component;


interface ResourceInterface
{
    /**
     * Returns the unique id
     *
     * @return mixed
     */
    public function getId();
}